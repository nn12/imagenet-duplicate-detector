import shutil
from pathlib import Path
from typing import Callable, Iterable, TypeAlias

import torch
from PIL import Image
from lshashpy3 import LSHash
from torch import Tensor
from torchvision import transforms
from torchvision.models import MobileNet_V2_Weights, MobileNetV2
from torchvision.models.feature_extraction import create_feature_extractor

transform: Callable[[Image.Image], Tensor] = transforms.Compose([
    transforms.
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])


def preprocess(image: Image.Image) -> Tensor:
    return transform(image).unsqueeze(0)


Descriptor: TypeAlias = tuple[float, ...]


class DescriptorExtractor:
    DESCRIPTOR_SIZE = 1280
    LAYER_NAME = 'adaptive_avg_pool2d'

    def __init__(self) -> None:
        model: MobileNetV2 = torch.hub.load(
            'pytorch/vision:v0.10.0',
            model='mobilenet_v2',
            weights=MobileNet_V2_Weights.DEFAULT
        )
        model.eval()

        self._feature_extractor = create_feature_extractor(model, return_nodes=[self.LAYER_NAME])

    def __call__(self, image: Image.Image) -> Descriptor:
        extracted: dict[str, Tensor] = self._feature_extractor(preprocess(image))
        descriptor: Tensor = extracted[self.LAYER_NAME]
        return tuple(descriptor.squeeze().tolist())


HASH_SIZE = 3
NUM_HASHTABLES = 12

COSINE_DISTANCE = 'cosine'
COSINE_DISTANCE_THRESHOLD = 0.3

HASHTABLE_PATH = 'lshash_hashtable.npz'
MATRICES_PATH = 'lshash_matrices.npz'

IMAGES_PATH = Path(r'example')


def query(
        lsh: LSHash,
        descriptor: Descriptor,
        distance_function: str = COSINE_DISTANCE,
        distance_threshold: float = COSINE_DISTANCE_THRESHOLD
) -> Iterable[Descriptor]:
    return {
        result[0][0]  # result ::= ((descriptor, extra_data), distance)
        for result in lsh.query(descriptor, distance_func=distance_function)
        if result[1] < distance_threshold
    }


def find_duplicates(lsh: LSHash, descriptors: Iterable[Descriptor]) -> dict[Descriptor, frozenset[Descriptor]]:
    groups: dict[Descriptor, frozenset[Descriptor]] = {}

    for descriptor in descriptors:
        duplicates: set[Descriptor] = {descriptor, *query(lsh, descriptor)}

        duplicates = duplicates.union(*filter(bool, map(groups.get, duplicates)))
        duplicates: frozenset[Descriptor] = frozenset(duplicates)

        for duplicate_descriptor in duplicates:
            groups[duplicate_descriptor] = duplicates

    return groups


def group_duplicates(
        results_folder: Path,
        groups: Iterable[Iterable[Descriptor]],
        paths: dict[Descriptor, str]
) -> None:
    for i, duplicates in enumerate(groups):
        group_folder = results_folder / f'group_{i}'
        shutil.rmtree(str(group_folder), ignore_errors=True)
        group_folder.mkdir(parents=True, exist_ok=True)

        for image_path in map(Path, map(paths.get, duplicates)):
            shutil.copy(str(image_path), str(group_folder / image_path.name))


@torch.no_grad()
def main() -> None:
    get_descriptor = DescriptorExtractor()
    lsh = LSHash(
        hash_size=HASH_SIZE,
        input_dim=DescriptorExtractor.DESCRIPTOR_SIZE,
        num_hashtables=NUM_HASHTABLES,
        matrices_filename=MATRICES_PATH,
        hashtable_filename=HASHTABLE_PATH,
        overwrite=True
    )

    image_paths: Iterable[str] = list(map(str, filter(Path.is_file, IMAGES_PATH.iterdir())))

    images: dict[str, Image.Image] = {path: Image.open(path).convert('RGB') for path in image_paths}
    paths: dict[Descriptor, str] = {get_descriptor(image): path for path, image in images.items()}
    descriptors = paths.keys()

    for descriptor in descriptors:
        lsh.index(descriptor)

    groups: dict[Descriptor, frozenset[Descriptor]] = find_duplicates(lsh, descriptors)

    results_folder = IMAGES_PATH / f'duplicates ({HASH_SIZE=}, {NUM_HASHTABLES=}, {COSINE_DISTANCE_THRESHOLD=})'

    group_duplicates(results_folder, groups=set(groups.values()), paths=paths)

    print(f'Results written to {results_folder}')


if __name__ == '__main__':
    main()
